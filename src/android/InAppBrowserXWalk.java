package de.neofonie.cordova.plugin.inappbrowserxwalk;

import android.annotation.SuppressLint;

import org.crosswalk.engine.XWalkCordovaWebView;

import android.content.Context;
import android.content.Intent;
import android.content.res.AssetManager;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.text.Html;
import android.text.InputType;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager.LayoutParams;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.Config;
import org.apache.cordova.CordovaArgs;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.LOG;
import org.apache.cordova.PluginResult;
import org.json.JSONException;
import org.json.JSONObject;
import org.xwalk.core.XWalkNavigationHistory;
import org.xwalk.core.XWalkView;
import org.xwalk.core.internal.XWalkCookieManager;

import java.awt.font.TextAttribute;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.StringTokenizer;

@SuppressWarnings("ALL")
@SuppressLint("SetJavaScriptEnabled")
public class InAppBrowserXWalk extends CordovaPlugin {


    protected static final String LOG_TAG = "InAppBrowserXWalk";
    private static final String NULL = "null";
    private static final String CLEAR_ALL_CACHE = "clearcache";
    private static final String CLEAR_SESSION_CACHE = "clearsessioncache";
    // Browser targets
    private static final String SELF = "_self";
    private static final String SYSTEM = "_system";
    private static final String BLANK = "_blank";
    private static final String EXIT_EVENT = "exit";
    private static final String LOCATION = "location";
    private static final String HIDDEN = "hidden";
    // Assets
    private static String ASSETS_LOCATION_IMAGES = "www/";
    private static String ASSETS_LOCATION_FONTS = "fonts/";
    private static String BUTTON_ICONFONT = "inappbrowser_iconfont.ttf";
    private static String BUTTON_CLOSE_ID = "inappbrowser_button__close";
    private static String BUTTON_CLOSE_CAPTION = "Dismiss";
    private static String BUTTON_CLOSE_ICON = "\uf128";
    private static String BUTTON_BACK_ID = "inappbrowser_button__back";
    private static String BUTTON_BACK_CAPTION = "Back";
    private static String BUTTON_BACK_ICON = "\uf360";
    private static String BUTTON_FORWARD_ID = "inappbrowser_button__forward";
    private static String BUTTON_FORWARD_CAPTION = "Forward";
    private static String BUTTON_FORWARD_ICON = "\uf362";
    // Main Views
    private InAppBrowserXWalkWindow inappbrowserWindow;
    private XWalkView inappbrowserWebview;
    private EditText textLocationbar;
    // Constants
    private CallbackContext callbackContext;
    // Defaults
    private boolean showLocationBar = false;
    private boolean openWindowHidden = false;
    private boolean clearAllCache = true;
    private boolean clearSessionCache = true;

    /**
     * Executes the request and returns PluginResult.
     *
     * @param action     The action to execute.
     * @param args       JSONArry of arguments for the plugin.
     * @param callbackId The callback id used when calling back into JavaScript.
     * @return A PluginResult object with a status and message.
     */
    public boolean execute(String action, CordovaArgs args, final CallbackContext callbackContext) throws JSONException {
        if (action.equals("open")) {
            this.callbackContext = callbackContext;
            final String url = args.getString(0);
            String t = args.optString(1);
            if (t == null || t.equals("") || t.equals(NULL)) {
                t = SELF;
            }
            final String target = t;
            final HashMap<String, Boolean> features = parseFeature(args.optString(2));

            Log.d(LOG_TAG, "target = " + target);

            this.cordova.getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    String result = "";
                    // SELF
                    if (SELF.equals(target)) {
                        Log.d(LOG_TAG, "in self");
                        // load in webview
                        if (url.startsWith("file://") || url.startsWith("javascript:") || Config.isUrlWhiteListed(url)) {
                            Log.d(LOG_TAG, "loading in webview");
                            webView.loadUrl(url);
                        }
                        //Load the dialer
                        else if (url.startsWith("tel:")) {
                            try {
                                Log.d(LOG_TAG, "loading in dialer");
                                Intent intent = new Intent(Intent.ACTION_DIAL);
                                intent.setData(Uri.parse(url));
                                cordova.getActivity().startActivity(intent);
                            } catch (android.content.ActivityNotFoundException e) {
                                LOG.e(LOG_TAG, "Error dialing " + url + ": " + e.toString());
                            }
                        }
                        // load in InAppBrowserXWalk
                        else {
                            Log.d(LOG_TAG, "loading in InAppBrowserXWalk");
                            result = showWebPage(url, features);
                        }
                    }
                    // SYSTEM
                    else if (SYSTEM.equals(target)) {
                        Log.d(LOG_TAG, "in system");
                        result = openExternal(url);
                    }
                    // BLANK - or anything else
                    else {
                        Log.d(LOG_TAG, "in blank");
                        result = showWebPage(url, features);
                    }

                    PluginResult pluginResult = new PluginResult(PluginResult.Status.OK, result);
                    pluginResult.setKeepCallback(true);
                    callbackContext.sendPluginResult(pluginResult);
                }
            });
        } else if (action.equals("close")) {
            // without runOnUiThread there es an error when trying to close window
            this.cordova.getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    closeDialog();
                }
            });
        } else if (action.equals("injectScriptCode")) {
            String jsWrapper = null;
            if (args.getBoolean(1)) {
                jsWrapper = String.format("prompt(JSON.stringify([eval(%%s)]), 'gap-iab-xwalk://%s')", callbackContext.getCallbackId());
            }
            injectDeferredObject(args.getString(0), jsWrapper);
        } else if (action.equals("injectScriptFile")) {
            String jsWrapper;
            if (args.getBoolean(1)) {
                jsWrapper = String.format("(function(d) { var c = d.createElement('script'); c.src = %%s; c.onload = function() { prompt('', 'gap-iab-xwalk://%s'); }; d.body.appendChild(c); })(document)", callbackContext.getCallbackId());
            } else {
                jsWrapper = "(function(d) { var c = d.createElement('script'); c.src = %s; d.body.appendChild(c); })(document)";
            }
            injectDeferredObject(args.getString(0), jsWrapper);
        } else if (action.equals("injectStyleCode")) {
            String jsWrapper;
            if (args.getBoolean(1)) {
                jsWrapper = String.format("(function(d) { var c = d.createElement('style'); c.innerHTML = %%s; d.body.appendChild(c); prompt('', 'gap-iab-xwalk://%s');})(document)", callbackContext.getCallbackId());
            } else {
                jsWrapper = "(function(d) { var c = d.createElement('style'); c.innerHTML = %s; d.body.appendChild(c); })(document)";
            }
            injectDeferredObject(args.getString(0), jsWrapper);
        } else if (action.equals("injectStyleFile")) {
            String jsWrapper;
            if (args.getBoolean(1)) {
                jsWrapper = String.format("(function(d) { var c = d.createElement('link'); c.rel='stylesheet'; c.type='text/css'; c.href = %%s; d.head.appendChild(c); prompt('', 'gap-iab-xwalk://%s');})(document)", callbackContext.getCallbackId());
            } else {
                jsWrapper = "(function(d) { var c = d.createElement('link'); c.rel='stylesheet'; c.type='text/css'; c.href = %s; d.head.appendChild(c); })(document)";
            }
            injectDeferredObject(args.getString(0), jsWrapper);
        } else if (action.equals("show")) {
            this.cordova.getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    inappbrowserWindow.show();
                }
            });
            PluginResult pluginResult = new PluginResult(PluginResult.Status.OK);
            pluginResult.setKeepCallback(true);
            this.callbackContext.sendPluginResult(pluginResult);
        } else {
            return false;
        }
        return true;
    }

    /**
     * Called when the view navigates.
     */
    @Override
    public void onReset() {
        closeDialog();
    }

    /**
     * Called by AccelBroker when listener is to be shut down.
     * Stop listener.
     */
    public void onDestroy() {
        closeDialog();
    }

    /**
     * Inject an object (script or style) into the InAppBrowserXWalk WebView.
     * <p/>
     * This is a helper method for the inject{Script|Style}{Code|File} API calls, which
     * provides a consistent method for injecting JavaScript code into the document.
     * <p/>
     * If a wrapper string is supplied, then the source string will be JSON-encoded (adding
     * quotes) and wrapped using string formatting. (The wrapper string should have a single
     * '%s' marker)
     *
     * @param source    The source object (filename or script/style text) to inject into
     *                  the document.
     * @param jsWrapper A JavaScript string to wrap the source string in, so that the object
     *                  is properly injected, or null if the source string is JavaScript text
     *                  which should be executed directly.
     */
    private void injectDeferredObject(String source, String jsWrapper) {
        String scriptToInject;
        if (jsWrapper != null) {
            org.json.JSONArray jsonEsc = new org.json.JSONArray();
            jsonEsc.put(source);
            String jsonRepr = jsonEsc.toString();
            String jsonSourceString = jsonRepr.substring(1, jsonRepr.length() - 1);
            scriptToInject = String.format(jsWrapper, jsonSourceString);
        } else {
            scriptToInject = source;
        }
        final String finalScriptToInject = scriptToInject;
        this.cordova.getActivity().runOnUiThread(new Runnable() {
            @SuppressLint("NewApi")
            @Override
            public void run() {
                inappbrowserWebview.evaluateJavascript(finalScriptToInject, null);
            }
        });
    }

    /**
     * Put the list of features into a hash map
     *
     * @param optString
     * @return
     */
    private HashMap<String, Boolean> parseFeature(String optString) {
        if (optString.equals(NULL)) {
            return null;
        } else {
            HashMap<String, Boolean> map = new HashMap<String, Boolean>();
            StringTokenizer features = new StringTokenizer(optString, ",");
            StringTokenizer option;
            while (features.hasMoreElements()) {
                option = new StringTokenizer(features.nextToken(), "=");
                if (option.hasMoreElements()) {
                    String key = option.nextToken();
                    if (key.equalsIgnoreCase(BUTTON_CLOSE_CAPTION)) {
                        this.BUTTON_CLOSE_CAPTION = option.nextToken();
                    } else {
                        Boolean value = option.nextToken().equals("no") ? Boolean.FALSE : Boolean.TRUE;
                        map.put(key, value);
                    }
                }
            }
            return map;
        }
    }

    /**
     * Display a new browser with the specified URL.
     *
     * @param url         The url to load.
     * @param usePhoneGap Load url in PhoneGap webview
     * @return "" if ok, or error message.
     */
    public String openExternal(String url) {
        try {
            Intent intent = null;
            intent = new Intent(Intent.ACTION_VIEW);
            // Omitting the MIME type for file: URLs causes "No Activity found to handle Intent".
            // Adding the MIME type to http: URLs causes them to not be handled by the downloader.
            Uri uri = Uri.parse(url);
            if ("file".equals(uri.getScheme())) {
                intent.setDataAndType(uri, webView.getResourceApi().getMimeType(uri));
            } else {
                intent.setData(uri);
            }
            this.cordova.getActivity().startActivity(intent);
            return "";
        } catch (android.content.ActivityNotFoundException e) {
            Log.d(LOG_TAG, "InAppBrowserXWalk: Error loading url " + url + ":" + e.toString());
            return e.toString();
        }
    }

    /**
     * Closes the dialog
     */
    public void closeDialog() {

        final XWalkView childView = this.inappbrowserWebview;

        if (childView == null) {
            return;
        }


        ViewGroup parentView = (ViewGroup) childView.getParent();
        parentView.removeView(childView);

        childView.clearCache(true);
        childView.destroyDrawingCache();
        childView.onDestroy();

        inappbrowserWindow.dismiss();

        try {
            JSONObject obj = new JSONObject();
            obj.put("type", EXIT_EVENT);
            sendUpdate(obj, false);
        } catch (JSONException ex) {
            Log.d(LOG_TAG, "Should never happen");
        }
    }

    /**
     * Checks to see if it is possible to go back one page in history, then does so.
     */
    private void goBack() {
        if (this.inappbrowserWebview.getNavigationHistory().canGoBack()) {
            this.inappbrowserWebview.getNavigationHistory().navigate(XWalkNavigationHistory.Direction.BACKWARD, 1);
        }
    }

    /**
     * Checks to see if it is possible to go forward one page in history, then does so.
     */
    private void goForward() {
        if (this.inappbrowserWebview.getNavigationHistory().canGoForward()) {
            this.inappbrowserWebview.getNavigationHistory().navigate(XWalkNavigationHistory.Direction.FORWARD, 1);
        }
    }

    /**
     * Navigate to the new page
     *
     * @param url to load
     */
    private void navigate(String url) {
        InputMethodManager imm = (InputMethodManager) this.cordova.getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(textLocationbar.getWindowToken(), 0);

        if (!url.startsWith("http") && !url.startsWith("file:")) {
            this.inappbrowserWebview.load("http://" + url, "");
        } else {
            this.inappbrowserWebview.load(url, null);
        }
        this.inappbrowserWebview.requestFocus();
    }


    /**
     * Should we show the location bar?
     *
     * @return boolean
     */
    private boolean getShowLocationBar() {
        return this.showLocationBar;
    }

    private InAppBrowserXWalk getInAppBrowserXWalk() {
        return this;
    }

    /**
     * Display a new browser with the specified URL.
     *
     * @param url The url to load.
     */
    public String showWebPage(final String url, HashMap<String, Boolean> features) {
        // Determine if we should hide the location bar.
        showLocationBar = true;
        openWindowHidden = false;
        if (features != null) {
            Boolean show = features.get(LOCATION);
            if (show != null) {
                showLocationBar = show.booleanValue();
            }
            Boolean hidden = features.get(HIDDEN);
            if (hidden != null) {
                openWindowHidden = hidden.booleanValue();
            }
            Boolean cache = features.get(CLEAR_ALL_CACHE);
            if (cache != null) {
                clearAllCache = cache.booleanValue();
            } else {
                cache = features.get(CLEAR_SESSION_CACHE);
                if (cache != null) {
                    clearSessionCache = cache.booleanValue();
                }
            }
        }

        final XWalkCordovaWebView thatWebView = (XWalkCordovaWebView) this.webView;

        // Create dialog in new thread
        Runnable runnable = new Runnable() {
            /**
             * Convert our DIP units to Pixels
             *
             * @return int
             */
            private int dpToPixels(int dipValue) {
                int value = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,
                        (float) dipValue,
                        cordova.getActivity().getResources().getDisplayMetrics()
                );

                return value;
            }

            public void run() {

                /**
                 * Main Window
                 */
                inappbrowserWindow = new InAppBrowserXWalkWindow(cordova.getActivity());
                inappbrowserWindow.setInAppBrowserXWalk(getInAppBrowserXWalk());

                /**
                 * WebView
                 */
                inappbrowserWebview = new XWalkView(cordova.getActivity().getApplicationContext(), cordova.getActivity());
                inappbrowserWebview.setId(6);

                inappbrowserWebview.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));

                /**
                 * Cookies
                 */
                XWalkCookieManager inappbrowserCookieManager = new XWalkCookieManager();
                inappbrowserCookieManager.setAcceptCookie(true);
                inappbrowserCookieManager.setAcceptFileSchemeCookies(true);

                if (clearAllCache) {
                    inappbrowserCookieManager.removeAllCookie();
                } else if (clearSessionCache) {
                    inappbrowserCookieManager.removeSessionCookie();
                }

                /**
                 * ASSET MANAGEMENT
                 */
                Context inappbrowserContext = cordova.getActivity().getApplicationContext();
                AssetManager inappbrowserAssets = inappbrowserContext.getResources().getAssets();
                Resources resources = cordova.getActivity().getResources();

                Typeface inappbrowserFont = Typeface.createFromAsset(inappbrowserAssets, ASSETS_LOCATION_FONTS + BUTTON_ICONFONT);

                String buttonCloseImagefile = ASSETS_LOCATION_IMAGES + resources.getString(resources.getIdentifier("inappbrowserxwalk_button__close", "string", cordova.getActivity().getPackageName()));
                String buttonBackImagefile = ASSETS_LOCATION_IMAGES + resources.getString(resources.getIdentifier("inappbrowserxwalk_button__back", "string", cordova.getActivity().getPackageName()));
                String buttonForwardImagefile = ASSETS_LOCATION_IMAGES + resources.getString(resources.getIdentifier("inappbrowserxwalk_button__back", "string", cordova.getActivity().getPackageName()));


                /**
                 *  LAYOUT CONTAINERS
                 */

                /**
                 *  Window
                 */
                RelativeLayout InAppBrowserMainContainer = new RelativeLayout(cordova.getActivity());

                InAppBrowserMainContainer.setLayoutParams(new RelativeLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));

                /**
                 *  Toolbar
                 */
                RelativeLayout inappbrowserToolbarContainer = new RelativeLayout(cordova.getActivity());
                inappbrowserToolbarContainer.setBackgroundColor(android.graphics.Color.TRANSPARENT);

                inappbrowserToolbarContainer.setLayoutParams(new RelativeLayout.LayoutParams(LayoutParams.MATCH_PARENT, this.dpToPixels(60)));
                inappbrowserToolbarContainer.setHorizontalGravity(Gravity.LEFT);
                inappbrowserToolbarContainer.setVerticalGravity(Gravity.TOP);

                /**
                 * Buttons
                 */
                RelativeLayout inappbrowserButtonContainer = new RelativeLayout(cordova.getActivity());
                inappbrowserButtonContainer.setId(1);

                inappbrowserButtonContainer.setLayoutParams(new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.MATCH_PARENT));
                inappbrowserButtonContainer.setHorizontalGravity(Gravity.LEFT);
                inappbrowserButtonContainer.setVerticalGravity(Gravity.CENTER_VERTICAL);


                /**
                 * CONTROLS
                 */

                /**
                 * Close
                 */
                Button buttonClose = new Button(cordova.getActivity());
                buttonClose.setId(5);
                buttonClose.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        closeDialog();
                    }
                });
                buttonClose.setContentDescription(BUTTON_CLOSE_CAPTION);

                buttonClose.setBackgroundColor(android.graphics.Color.TRANSPARENT);
                buttonClose.setTypeface(inappbrowserFont);
                buttonClose.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 60);
                buttonClose.setIncludeFontPadding(false);
                buttonClose.setPadding(0,0,0,0);
                try {
                    InputStream assetStream = inappbrowserAssets.open(buttonCloseImagefile);
                    Drawable buttonCloseIcon = Drawable.createFromStream(assetStream, null);

                    buttonClose.setBackgroundDrawable(buttonCloseIcon);
                } catch (IOException ex) {
                    buttonClose.setText(BUTTON_CLOSE_ICON);
                }

                RelativeLayout.LayoutParams buttonCloseLayoutParams = new RelativeLayout.LayoutParams(this.dpToPixels(60), LayoutParams.MATCH_PARENT);
                buttonCloseLayoutParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
                buttonClose.setLayoutParams(buttonCloseLayoutParams);


                /**
                 * Back
                 */
                Button buttonBack = new Button(cordova.getActivity());
                buttonBack.setId(2);
                buttonBack.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        goBack();
                    }
                });
                buttonBack.setContentDescription(BUTTON_BACK_CAPTION);

                buttonBack.setBackgroundColor(android.graphics.Color.TRANSPARENT);
                buttonBack.setTypeface(inappbrowserFont);
                buttonBack.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 60);
                buttonBack.setIncludeFontPadding(false);
                buttonBack.setPadding(0,0,0,0);
                try {
                    InputStream assetStream = inappbrowserAssets.open(buttonBackImagefile);
                    Drawable buttonBackIcon = Drawable.createFromStream(assetStream, null);

                    buttonBack.setBackground(buttonBackIcon);

                } catch (IOException ex) {
                    buttonBack.setText(BUTTON_BACK_ICON);
                }

                RelativeLayout.LayoutParams buttonBackLayoutParams = new RelativeLayout.LayoutParams(this.dpToPixels(60), LayoutParams.MATCH_PARENT);
                buttonBackLayoutParams.addRule(RelativeLayout.ALIGN_LEFT);
                buttonBack.setLayoutParams(buttonBackLayoutParams);


                /**
                 * Forward
                 */
                Button buttonForward = new Button(cordova.getActivity());
                buttonForward.setId(3);
                buttonForward.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        goForward();
                    }
                });
                buttonForward.setContentDescription(BUTTON_FORWARD_CAPTION);

                buttonForward.setBackgroundColor(android.graphics.Color.TRANSPARENT);
                buttonForward.setTypeface(inappbrowserFont);
                buttonForward.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 60);
                buttonForward.setIncludeFontPadding(false);
                buttonForward.setPadding(0,0,0,0);
                try {
                    InputStream assetStream = inappbrowserAssets.open(buttonForwardImagefile);
                    Drawable buttonForwardIcon = Drawable.createFromStream(assetStream, null);

                    buttonForward.setBackgroundDrawable(buttonForwardIcon);
                } catch (IOException ex) {
                    buttonBack.setText(BUTTON_FORWARD_ICON);
                }

                RelativeLayout.LayoutParams buttonForwardLayoutParams = new RelativeLayout.LayoutParams(this.dpToPixels(60), LayoutParams.MATCH_PARENT);
                buttonForwardLayoutParams.addRule(RelativeLayout.RIGHT_OF, 2);
                buttonForward.setLayoutParams(buttonForwardLayoutParams);

                /**
                 * Location Bar
                 */
                textLocationbar = new EditText(cordova.getActivity());
                textLocationbar.setId(4);
                textLocationbar.setSingleLine(true);
                textLocationbar.setText(url);
                textLocationbar.setInputType(InputType.TYPE_TEXT_VARIATION_URI);
                textLocationbar.setImeOptions(EditorInfo.IME_ACTION_GO);
                textLocationbar.setInputType(InputType.TYPE_NULL);
                textLocationbar.setOnKeyListener(new View.OnKeyListener() {
                    public boolean onKey(View v, int keyCode, KeyEvent event) {
                        // If the event is a key-down event on the "enter" button
                        if ((event.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {
                            navigate(textLocationbar.getText().toString());
                            return true;
                        }
                        return false;
                    }
                });

                RelativeLayout.LayoutParams textLayoutParams = new RelativeLayout.LayoutParams(LayoutParams.MATCH_PARENT,LayoutParams.WRAP_CONTENT);
                textLayoutParams.addRule(RelativeLayout.RIGHT_OF, 1);
                textLayoutParams.addRule(RelativeLayout.LEFT_OF, 5);
                textLocationbar.setLayoutParams(textLayoutParams);


                /**
                 * Compose final layout
                 * TODO: Fix Adressbar, Navbutton scaling
                 */
                // Toolbar
                //inappbrowserButtonContainer.addView(buttonBack);
                //inappbrowserButtonContainer.addView(buttonForward);
                inappbrowserToolbarContainer.addView(inappbrowserButtonContainer);

                if (getShowLocationBar()) {
                    //inappbrowserToolbarContainer.addView(textLocationbar);
                }
                inappbrowserToolbarContainer.addView(buttonClose);

                // Webview
                InAppBrowserMainContainer.addView(inappbrowserWebview);
                InAppBrowserMainContainer.addView(inappbrowserToolbarContainer);

                // Window
                inappbrowserWindow.setContentView(InAppBrowserMainContainer);


                /**
                 * Webview Clients
                 */
                inappbrowserWebview.setUIClient(new InAppBrowserXWalkUIClient(thatWebView));
                inappbrowserWebview.setResourceClient(new InAppBrowserXWalkResourceClient(thatWebView, textLocationbar, cordova));


                /**
                 * Load URL
                 */
                inappbrowserWebview.load(url, null);
                inappbrowserWindow.show();
                if (openWindowHidden) {
                    inappbrowserWindow.hide();
                }

                /**
                 * Input focus
                 */
                inappbrowserWebview.requestFocus();
                inappbrowserWebview.requestFocusFromTouch();

            }
        };
        this.cordova.getActivity().runOnUiThread(runnable);
        return "";
    }

    /**
     * Create a new plugin success result and send it back to JavaScript
     *
     * @param obj a JSONObject contain event payload information
     */
    private void sendUpdate(JSONObject obj, boolean keepCallback) {
        sendUpdate(obj, keepCallback, PluginResult.Status.OK);
    }

    /**
     * Create a new plugin result and send it back to JavaScript
     *
     * @param obj    a JSONObject contain event payload information
     * @param status the status code to return to the JavaScript environment
     */
    private void sendUpdate(JSONObject obj, boolean keepCallback, PluginResult.Status status) {
        if (callbackContext != null) {
            PluginResult result = new PluginResult(status, obj);
            result.setKeepCallback(keepCallback);
            callbackContext.sendPluginResult(result);
            if (!keepCallback) {
                callbackContext = null;
            }
        }
    }

}
