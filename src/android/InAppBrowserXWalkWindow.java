/*
       Licensed to the Apache Software Foundation (ASF) under one
       or more contributor license agreements.  See the NOTICE file
       distributed with this work for additional information
       regarding copyright ownership.  The ASF licenses this file
       to you under the Apache License, Version 2.0 (the
       "License"); you may not use this file except in compliance
       with the License.  You may obtain a copy of the License at

         http://www.apache.org/licenses/LICENSE-2.0

       Unless required by applicable law or agreed to in writing,
       software distributed under the License is distributed on an
       "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
       KIND, either express or implied.  See the License for the
       specific language governing permissions and limitations
       under the License.
*/
package de.neofonie.cordova.plugin.inappbrowserxwalk;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;

import org.json.JSONException;
import org.json.JSONObject;
import org.xwalk.core.XWalkPreferences;

/**
 * Created by Oliver on 22/11/2013.
 */
public class InAppBrowserXWalkWindow extends Dialog {
    Context context;
    InAppBrowserXWalk inAppBrowserXWalk = null;

    public InAppBrowserXWalkWindow(Context context) {
        super(context, android.R.style.Theme_NoTitleBar_Fullscreen);
        this.context = context;


        // XWalkPreferences
        XWalkPreferences.setValue(XWalkPreferences.JAVASCRIPT_CAN_OPEN_WINDOW, true);
        XWalkPreferences.setValue(XWalkPreferences.SUPPORT_MULTIPLE_WINDOWS, true);
        XWalkPreferences.setValue(XWalkPreferences.REMOTE_DEBUGGING, true);
        //XWalkPreferences.setValue(XWalkPreferences.ANIMATABLE_XWALK_VIEW, true);

        this.requestWindowFeature(Window.FEATURE_NO_TITLE);

        WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();
        layoutParams.copyFrom(this.getWindow().getAttributes());

        layoutParams.width = WindowManager.LayoutParams.MATCH_PARENT;
        layoutParams.height = WindowManager.LayoutParams.MATCH_PARENT;
        layoutParams.windowAnimations = android.R.style.Animation_Toast;
        this.getWindow().setAttributes(layoutParams);

        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);

        this.setCancelable(true);
    }

    public void setInAppBrowserXWalk(InAppBrowserXWalk browser) {
        this.inAppBrowserXWalk = browser;
    }

    public void onBackPressed () {
        if (this.inAppBrowserXWalk == null) {
            //this.dismiss();
            this.inAppBrowserXWalk.closeDialog();
        } else {
            // better to go through the in inAppBrowserXWalk
            // because it does a clean up
           this.inAppBrowserXWalk.closeDialog();
            //this.dismiss();
        }
    }
}
