package de.neofonie.cordova.plugin.inappbrowserxwalk;

import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import android.widget.EditText;

import org.apache.cordova.CordovaInterface;
import org.apache.cordova.LOG;
import org.apache.cordova.PluginResult;
import org.crosswalk.engine.XWalkCordovaWebView;
import org.json.JSONException;
import org.json.JSONObject;
import org.xwalk.core.XWalkResourceClient;
import org.xwalk.core.XWalkView;

public class InAppBrowserXWalkResourceClient extends XWalkResourceClient {

    private static final String LOAD_START_EVENT = "loadstart";
    private static final String LOAD_ERROR_EVENT = "loaderror";
    XWalkCordovaWebView mainCordovaWebview;
    EditText inappbrowserLocationbar;
    private String LOG_TAG = "InAppBrowserXWalkResourceClient";
    private CordovaInterface cordova;

    /**
     * Constructor.
     *
     * @param hostView  Main Cordova View
     * @param textLabel The InAppBrowser address bar
     */
    public InAppBrowserXWalkResourceClient(XWalkCordovaWebView hostView, EditText textLabel, CordovaInterface cdvInterface) {
        super(hostView.getView());
        this.mainCordovaWebview = hostView;
        this.inappbrowserLocationbar = textLabel;
        this.cordova = cdvInterface;
    }

    /**
     * @param view        The WebView that is initiating the callback.
     * @param errorCode   The error code corresponding to an ERROR_* value.
     * @param description A String describing the error.
     * @param failingUrl  The url that failed to load.
     */
    @Override
    public void onReceivedLoadError(XWalkView view, int errorCode, String description, String failingUrl) {
        try {
            JSONObject obj = new JSONObject();
            obj.put("type", LOAD_ERROR_EVENT);
            obj.put("url", failingUrl);
            obj.put("code", errorCode);
            obj.put("message", description);

            PluginResult result = new PluginResult(PluginResult.Status.ERROR, obj);
            this.mainCordovaWebview.sendPluginResult(result, failingUrl);

        } catch (JSONException ex) {
            Log.d(LOG_TAG, "Should never happen");
        }
    }


    /**
     * Notify the host application that a page has started loading.
     *
     * @param view The webview initiating the callback.
     * @param url  The url of the page.
     */
    @Override
    public void onLoadStarted(XWalkView view, String url) {
        String locationCurrent = inappbrowserLocationbar.getText().toString();
        String locationNew = "";

        // Standard protocols
        if (url.startsWith("http:") || url.startsWith("https:") || url.startsWith("file:")) {
            locationNew = url;
        }
        // Special protocols
        else if (url.startsWith("tel:")) {
            try {
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse(url));
                this.cordova.getActivity().startActivity(intent);
            } catch (android.content.ActivityNotFoundException e) {
                LOG.e(LOG_TAG, "Error dialing " + url + ": " + e.toString());
            }
        } else if (url.startsWith("geo:") || url.startsWith("tel:") || url.startsWith("market:")) {
            try {
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse(url));
                this.cordova.getActivity().startActivity(intent);
            } catch (android.content.ActivityNotFoundException e) {
                LOG.e(LOG_TAG, "Error with " + url + ": " + e.toString());
            }
        }

        // SMS/MMS protocols
        else if (url.startsWith("sms:")) {
            try {
                Intent intent = new Intent(Intent.ACTION_VIEW);

                // Get address
                String address;
                int parmIndex = url.indexOf('?');
                if (parmIndex == -1) {
                    address = url.substring(4);
                } else {
                    address = url.substring(4, parmIndex);

                    // If body, then set sms body
                    Uri uri = Uri.parse(url);
                    String query = uri.getQuery();
                    if (query != null) {
                        if (query.startsWith("body=")) {
                            intent.putExtra("sms_body", query.substring(5));
                        }
                    }
                }
                intent.setData(Uri.parse("sms:" + address));
                intent.putExtra("address", address);
                intent.setType("vnd.android-dir/mms-sms");
                this.cordova.getActivity().startActivity(intent);
            } catch (android.content.ActivityNotFoundException e) {
                LOG.e(LOG_TAG, "Error sending sms " + url + ":" + e.toString());
            }
        } else {
            locationNew = "http://" + url;
        }

        if (!locationNew.equals(locationCurrent)) {
            inappbrowserLocationbar.setText(locationNew);
        }

        try {
            JSONObject obj = new JSONObject();
            obj.put("type", LOAD_START_EVENT);
            obj.put("url", locationNew);

            PluginResult result = new PluginResult(PluginResult.Status.OK, obj);
            this.mainCordovaWebview.sendPluginResult(result, locationNew);

        } catch (JSONException ex) {
            Log.d(LOG_TAG, "Should never happen");
        }
    }

}
