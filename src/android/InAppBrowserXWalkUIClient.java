package de.neofonie.cordova.plugin.inappbrowserxwalk;

import org.apache.cordova.LOG;
import org.apache.cordova.PluginResult;
import org.crosswalk.engine.XWalkCordovaWebView;
import org.json.JSONArray;
import org.json.JSONException;
import org.xwalk.core.XWalkJavascriptResult;
import org.xwalk.core.XWalkUIClient;
import org.xwalk.core.XWalkView;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.net.Uri;
import android.view.KeyEvent;
import android.webkit.ValueCallback;

public class InAppBrowserXWalkUIClient extends XWalkUIClient {

    private XWalkCordovaWebView mainCordovaWebview;


    /**
     * Constructor.
     *
     * @param view Main Cordova View
     */
    public InAppBrowserXWalkUIClient(XWalkCordovaWebView view) {
        super(view.getView());
        this.mainCordovaWebview = view;
    }

    @Override
    public boolean onJavascriptModalDialog(XWalkView view, JavascriptMessageType type, String url,
                                           String message, String defaultValue, XWalkJavascriptResult result) {
        switch (type) {
            case JAVASCRIPT_ALERT:
                return onJsAlert(view, url, message, result);
            case JAVASCRIPT_CONFIRM:
                return onJsConfirm(view, url, message, result);
            case JAVASCRIPT_PROMPT:
                return onJsPrompt(view, url, message, defaultValue, result);
            case JAVASCRIPT_BEFOREUNLOAD:
                // Reuse onJsConfirm to show the dialog.
                return onJsConfirm(view, url, message, result);
            default:
                break;
        }

        return false;
    }


    /**
     * Display a alert dialog.
     */
    private boolean onJsAlert(XWalkView view, String url, String message, final XWalkJavascriptResult result) {
        AlertDialog.Builder dlg = new AlertDialog.Builder(view.getContext());
        dlg.setMessage(message);
        dlg.setTitle("Alert");
        //Don't let alerts break the back button
        dlg.setCancelable(true);
        dlg.setPositiveButton(android.R.string.ok,
                new AlertDialog.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        result.confirm();
                    }
                });
        dlg.setOnCancelListener(
                new DialogInterface.OnCancelListener() {
                    public void onCancel(DialogInterface dialog) {
                        result.cancel();
                    }
                });
        dlg.setOnKeyListener(new DialogInterface.OnKeyListener() {
            //DO NOTHING
            public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    result.confirm();
                    return false;
                } else
                    return true;
            }
        });
        dlg.create();
        dlg.show();
        return true;
    }


    /**
     * Display a confirm dialog.
     */
    private boolean onJsConfirm(XWalkView view, String url, String message, final XWalkJavascriptResult result) {
        return super.onJavascriptModalDialog(view, null, url, message, null, result);
    }


    /**
     * Tell the client to display a prompt dialog to the user.
     * If the client returns true, WebView will assume that the client will
     * handle the prompt dialog and call the appropriate JsPromptResult method.
     * <p/>
     * The prompt bridge provided for the InAppBrowserXWalk is capable of executing any
     * outstanding callback belonging to the InAppBrowserXWalk plugin. Care has been
     * taken that other callbacks cannot be triggered, and that no other code
     * execution is possible.
     * <p/>
     * To trigger the bridge, the prompt default value should be of the form:
     * <p/>
     * gap-iab-xwalk://<callbackId>
     * <p/>
     * where <callbackId> is the string id of the callback to trigger (something
     * like "InAppBrowserXWalk0123456789")
     * <p/>
     * If present, the prompt message is expected to be a JSON-encoded value to
     * pass to the callback. A JSON_EXCEPTION is returned if the JSON is invalid.
     */
    private boolean onJsPrompt(XWalkView view, String url, String message, String defaultValue,
                               final XWalkJavascriptResult result) {
        // See if the prompt string uses the 'gap-iab-xwalk' protocol. If so, the remainder should be the id of a callback to execute.
        if (defaultValue != null && defaultValue.startsWith("gap")) {
            if (defaultValue.startsWith("gap-iab-xwalk://")) {
                PluginResult scriptResult;
                String scriptCallbackId = defaultValue.substring(10);
                if (scriptCallbackId.startsWith("InAppBrowserXWalk")) {
                    if (message == null || message.length() == 0) {
                        scriptResult = new PluginResult(PluginResult.Status.OK, new JSONArray());
                    } else {
                        try {
                            scriptResult = new PluginResult(PluginResult.Status.OK, new JSONArray(message));
                        } catch (JSONException e) {
                            scriptResult = new PluginResult(PluginResult.Status.JSON_EXCEPTION, e.getMessage());
                        }
                    }
                    this.mainCordovaWebview.sendPluginResult(scriptResult, scriptCallbackId);
                    result.confirmWithResult("");
                    return true;
                }
            } else {
                // Anything else with a gap: prefix should get this message
                String LOG_TAG = "InAppBrowserXWalkUIClient";
                LOG.w(LOG_TAG, "InAppBrowserXWalk does not support Cordova API calls: " + url + " " + defaultValue);
                result.cancel();
                return true;
            }
        }
        return false;
    }


    // File Chooser
    @Override
    public void openFileChooser(XWalkView view, ValueCallback<Uri> uploadFile, String acceptType, String capture) {
        super.openFileChooser(view, uploadFile, acceptType, capture);
    }

}
