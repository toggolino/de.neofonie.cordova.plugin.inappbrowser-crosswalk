# de.neofonie.cordova.plugin.inappbrowser-crosswalk

This plugin provides a Crosswalk-based web browser view that displays when calling `window.plugins.InAppBrowserXWalk.open()`.

## Installation

cordova plugin add org.apache.cordova.inappbrowser